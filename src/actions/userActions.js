import * as basicActions from './index';
//- api
import * as api from '../services/api';
import _ from 'lodash';
import moment from 'moment';

const login = (phone, password) => {
    return (dispatch) => {
        //- loading
        dispatch(basicActions.getData());
        //- call api
        return api.login({
            phone: phone,
            password: password
        })
        .then((response) => {
            if(_.size(response) > 0 && response !== undefined)
            {
                dispatch(basicActions.getDataSuccess(response));
            }else{
                dispatch(basicActions.getDataSuccess(null));
            }
        })
        .catch((err) => console.log('action error', err));

    }
}

const logout = (token) => {
    console.log('here at logout action')
    return (dispatch) => {
        //- loading
        dispatch(basicActions.getData());
        //- call api
        api.logout(token)
        .then((response) => {
            dispatch(basicActions.getDataSuccess(response));
            dispatch(basicActions.logout()); //- reset all data
        })
        .catch((err) => console.log('error', err));
    }
}

const checkIn = (params, token) => {
    return (dispatch) => {
        //- loading
        dispatch(basicActions.getData());
        api.checkIn(params, token)
        .then((response) => {
            console.log('here at checkin action with data', response);
            dispatch(basicActions.getDataSuccess(response));
            // dispatch(basicActions.updateShiftData(response.data));
        })
        .catch((err) => console.log('error', err));
    }
}

const checkOut = (params, token) => {
    return (dispatch) => {
        //- loading
        dispatch(basicActions.getData());
        api.checkOut(params, token)
        .then((response) => {
            console.log('here at checkin action with data', response);
            dispatch(basicActions.getDataSuccess(response));
            // dispatch(basicActions.updateShiftData(response.data));
        })
        .catch((err) => console.log('error', err));
    }
}

const updateShiftData = (shiftData) => {
    return dispatch => {
        //- loading
        dispatch(basicActions.getData());
        dispatch(basicActions.updateShiftData(shiftData));
    }
}

const getShift = (params, token) => {
    return (dispatch) => {
        //- loading
        dispatch(basicActions.getData());
        api.getShift(params, token)  
        .then((response) => {
            console.log('get shift...', response.data.data)
            dispatch(basicActions.getShiftList(response.data.data));
        })
        .catch((err) => console.log('error', err));
    }
}

const getNewShift = (params, token) => {
    return (dispatch) => {
        //- loading
        dispatch(basicActions.getData());
        api.getNewShift(params, token)  
        .then((response) => {
            console.log('get new shift...', response.data.data)
            dispatch(basicActions.getNewShiftList(response.data.data));
        })
        .catch((err) => console.log('error', err));
    }
}

const getShiftByWeek = (token) => {
    return (dispatch) => {
        //- loading
        dispatch(basicActions.getData());
        api.getShiftByWeek(token)
        .then((response) => {
            dispatch(basicActions.getScheduleList(response));
        })
        .catch((err) => console.log(err))
    }
}

const changePassword = (params, token) => {
    return dispatch => {
        //- loading
        dispatch(basicActions.getData());
        api.changePass(params, token)
        .then(response => {
            dispatch(basicActions.getDataSuccess(response));
        })
        .catch((err) => console.log(err))
    }
}

const changeShift = (selectedShiftID, shiftData) => {
    return dispatch => {
        //- loading
        dispatch(basicActions.getData());
        //- do an action
        console.log('selected shift id', selectedShiftID)
        console.log('shift data', shiftData)
        dispatch(basicActions.changeShift(selectedShiftID, shiftData));
    }
}

const getUserInfo = (token) => {
    return dispatch => {
        //- loading
        dispatch(basicActions.getData());
        api.getUserInfo(token)
        .then(response => {
            dispatch(basicActions.getUserInfo(response));
        })
        .catch((err) => console.log(err));
    }
}

const updatePhone = (params, token) => {
    return dispatch => {
        //- loading
        dispatch(basicActions.getData());
        api.updatePhone(params, token)
        .then(response => {
            dispatch(basicActions.getDataSuccess(response));
        })
        .catch((err) => console.log(err));
    }
}

const updateUsername = (params, token) => {
    return dispatch => {
        //- loading
        dispatch(basicActions.getData());
        api.updateUsername(params, token)
        .then(response => {
            dispatch(basicActions.getDataSuccess(response));
        })
        .catch((err) => console.log(err));
    }
}

const updateAvatar = (params, token) => {
    return dispatch => {
        //- loading
        dispatch(basicActions.getData());
        api.updateAvatar(params, token)
        .then(response => {
            dispatch(basicActions.getDataSuccess(response));
        })
        .catch((err) => console.log(err));
    }
}

const getNewShiftData = (newShiftData) => {
    return dispatch => {
        dispatch(basicActions.getNewShiftData(newShiftData));
    }
}

const getDayStatistic = (params, token) => {
    return dispatch => {
        //- loading
        dispatch(basicActions.getData());
        api.getDayStatistic(params, token)
        .then(response => {
            console.log('day statistic', response)
            dispatch(basicActions.getDayStatistic(response));
        })
        .catch((err) => console.log(err));
    }
}

export {
    login,
    logout,
    checkIn,
    checkOut,
    getShiftByWeek,
    changePassword,
    getShift,
    changeShift,
    getUserInfo,
    updatePhone,
    getNewShift,
    getNewShiftData,
    updateUsername,
    updateAvatar,
    getDayStatistic
}