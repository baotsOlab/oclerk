import {
    FETCHING_DATA,
    FETCHING_DATA_SUCCESS,
    FETCHING_DATA_FAIL,
    LOADING,
    SCHEDULE_LIST,
    SHIFT_LIST,
    CHANGE_SHIFT,
    LOGOUT,
    USER_INFO,
    NEW_SHIFT_LIST,
    NEW_SHIFT_DATA,
    DAY_STATISTIC
} from './types';


export const getData = () => {
    return {
        type: FETCHING_DATA
    }
}

export const showLoading = () => {
    return {
        type: LOADING
    }
}

export const getDataSuccess = (data) => {
    return {
        type: FETCHING_DATA_SUCCESS,
        data
    }
}

export const getScheduleList = (data) => {
    return {
        type: SCHEDULE_LIST,
        data
    }
}

export const getShiftList = (data) => {
    return {
        type: SHIFT_LIST,
        data
    }
}

export const changeShift = (selectedShiftID, shiftData) => {
    return {
        type: CHANGE_SHIFT,
        selectedShiftID: selectedShiftID,
        shiftData: shiftData
    }
}

export const logout = () => {
    return {
        type: LOGOUT
    }
}

export const getUserInfo = (userInfo) => {
    return {
        type: USER_INFO,
        userInfo: userInfo
    }
}

export const getNewShiftList = (newShiftList) => {
    return {
        type: NEW_SHIFT_LIST,
        newShiftList: newShiftList
    }
}

export const getNewShiftData = (newShiftData) => {
    return {
        type: NEW_SHIFT_DATA,
        newShiftData: newShiftData
    }
}

export const getDataFail = () => {
    return {
        type: FETCHING_DATA_FAIL
    }
}

export const getDayStatistic = (dayStatistic) => {
    return {
        type: DAY_STATISTIC,
        dayStatistic: dayStatistic
    }
}