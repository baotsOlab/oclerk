
const mainColor = "#00224B";
const title = "#000000";
const inactiveColor = "#A1A1A1";
const activeColor = "#00224B";
const bgColor = '#ffffff';
const subHeader1 = "#343A40";
const subHeader2 = "#A1A1A1";
const hrLine = "#979797";
const imageAreaColor = "#F8F8F8";

export {
    mainColor,
    title,
    inactiveColor,
    activeColor,
    bgColor,
    subHeader1,
    subHeader2,
    hrLine,
    imageAreaColor
}