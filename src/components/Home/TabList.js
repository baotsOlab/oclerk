import React, { Component } from 'react'
import { 
    Text, 
    View,
    StyleSheet,
    AsyncStorage,
    Button,
} from 'react-native'

import PropTypes from 'prop-types';
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import {
    getShift
} from '../../actions/userActions';

//- import singleTab component
import SingleTab from '../common/singleTab';
import _ from 'lodash';
import ImagePicker from 'react-native-image-crop-picker';

export class TabList extends Component {

    constructor(props)
    {
        super(props)
        this.state = {
            shiftData: [],
            selectedShiftID: 0
        }
    }

    static getDerivedStateFromProps(nextProps, nextState)
    {
        console.log('next props...', nextProps.shiftData)
        console.log('next state...', nextState)
        // if(this.state.shiftData !== nextProps.shiftData)
        // {
        //     console.log('new shift data', nextProps.shiftData)
        //     this.setState({
        //         shiftData: nextProps.shiftData
        //     })
        // }
        return null;
    }

    _openImagePicker = async () => {
        await ImagePicker.openCamera({
            width: 100,
            height: 100,
            cropping: true,
            compressImageMaxWidth: 100,
            compressImageMaxHeight:100,
            compressImageQuality: 0.5
        }).then(image => {
            console.log('image from image picker', image);
        });
    }

    render() {
        let {
            shiftData,
            selectedShiftID
        } = this.props;
        // console.log('in image', _.map(shiftData.attendances, 'in_image'))
        // console.log('is in image null?', _.isNull(_.map(shiftData.attendances, 'in_image')[0]));
        // console.log('is out image null?', _.isNull(_.map(shiftData.attendances, 'out_image')[0]));
        return (
            <View style={styles.container}>

                {/* Row 1 */}
                <View style={styles.row1}>
                    <SingleTab
                        tabName={'Check in'}
                        imageLink={'checkIn'}
                        navigation={this.props.navigation}
                        goTo={'ImageCapture'}
                        // goTo={'Camera'}
                        //- if checked
                        checked={(shiftData !== 0 && _.size(shiftData.attendances) > 0 && !_.isNull(_.map(shiftData.attendances, 'in_image')[0]) ) ? true : false}
                        imageURI={(shiftData !== 0) ? _.map(shiftData.attendances, 'in_image')[0] : ''}
                        checkedTime={(shiftData !== 0) ? _.map(shiftData.attendances, 'in_time')[0] : ''}
                    />
                    <SingleTab 
                        isMarginLeft={true}
                        left={19}
                        tabName={'Check out'}
                        imageLink={'checkOut'}
                        navigation={this.props.navigation}
                        goTo={'ImageCapture'}
                        // goTo={'Camera'}
                        //- if checked
                        checked={(shiftData !== 0 && _.size(shiftData.attendances) > 0 && !_.isNull(_.map(shiftData.attendances, 'out_image')[0]) ) ? true : false}
                        imageURI={(shiftData !== 0) ? _.map(shiftData.attendances, 'out_image')[0] : ''}
                        checkedTime={(shiftData !== 0) ? _.map(shiftData.attendances, 'out_time')[0] : ''}
                    />
                </View>

                {/* Row 2 */}
                {/*<View style={styles.row2}>
                    <SingleTab
                        tabName={'Start lunch'}
                        imageLink={'startLunch'}
                    />
                    <SingleTab 
                        isMarginLeft={true}
                        left={19}
                        tabName={'Finish lunch'}
                        imageLink={'finishLunch'}
                        checked={true}
                    />
                </View>*/}
            </View>
        )
    }
}

// export default TabList

//- redux
const mapStateToProps = (state) => ({
    selectedShiftID: (state.userReducer.selectedShiftID !== undefined) ? state.userReducer.selectedShiftID : 0,
    shiftData: (state.userReducer.shiftData !== undefined) ? state.userReducer.shiftData : 0
})
  
const mapDispatchToProps = (dispatch) => {
    return {
        getShift: bindActionCreators((params, token) => getShift(params, token), dispatch)
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(TabList)

const styles = StyleSheet.create({
    
    container: {
        flex: 1,
    },
    row1: {
        flex: 1,
        flexDirection: 'row'
    },
    row2: {
        flex: 1,
        flexDirection: 'row'
    },
    left: {
        marginLeft: 19,
    }

});