import React, { Component } from 'react'
import { 
    Text, 
    View,
    StyleSheet ,
    AsyncStorage
} from 'react-native'
import moment from 'moment';
import { Agenda } from 'react-native-calendars';
//- font
import {
    h5Font,
    body4
} from '../../utils/fonts';
import ScheduleSingleDate from '../../components/common/scheduleSingleDate';
import {
    bgColor
} from '../../utils/color';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
    getShiftByWeek
} from '../../actions/userActions';
import _ from 'lodash';

export class ViewAllSchedule extends Component {

    constructor(props)
    {
        super(props);
        this.state = {
            selectedDate: "",
            items: {}
        }  
        this._getShiftData();
    }

    _getShiftData = async () => {
        //- call shift by week event
        let userToken = await AsyncStorage.getItem('userToken');
        //- fire an action
        await this.props.getShiftByWeek(userToken);
    }

    render() {
        let {
            data
        } = this.props.scheduleList;
        let date =  (_.size(data) > 0) ? _.keys(data) : [];
        console.log('object keys....', _.keys(data))
        let newDate = _.zipObject(date, _.map(date, _.constant({marked: true, dotColor: '#00224B'})));
        // console.log('shift...', data);
        console.log('type of....', typeof data)
        console.log('new date....', newDate)
        return (
            <View style={styles.container}>

                <Agenda
                    // the list of items that have to be displayed in agenda. If you want to render item as empty date
                    // the value of date key kas to be an empty array []. If there exists no value for date key it is
                    // considered that the date in question is not yet loaded
                    // items={
                    //     {
                    //         '2018-10-22': [
                    //             {
                    //                 date: '22/10/2018',
                    //                 shift: 'Morning Shift',
                    //                 content: '8am - 12am'
                    //             },
                    //             {
                    //                 date: '22/10/2018',
                    //                 shift: 'Afternoon Shift',
                    //                 content: '8am - 12am'
                    //             }
                    //         ],
                    //         '2018-10-23': [
                    //             {
                    //                 date: '23/10/2018',
                    //                 shift: 'Morning Shift',
                    //                 content: '8am - 12am'
                    //             },
                    //             {
                    //                 date: '23/10/2018',
                    //                 shift: 'Afternoon Shift',
                    //                 content: '8am - 12am'
                    //             }
                    //         ],
                    //         '2018-10-25': [
                    //             {
                    //                 date: '25/10/2018',
                    //                 shift: 'Morning Shift',
                    //                 content: '8am - 12am'
                    //             },
                    //             {
                    //                 date: '25/10/2018',
                    //                 shift: 'Afternoon Shift',
                    //                 content: '8am - 12am'
                    //             }
                    //         ],
                    //     }
                    // }
                    items={data}
                    // callback that gets called on day press
                    onDayPress={(day)=>{
                        console.log('selected date', day);
                        this.setState({
                            selectedDate: day.dateString
                        })
                    }}
                    // initially selected day
                    selected={moment().format('YYYY-MM-DD')}
                    // specify how each item should be rendered in agenda
                    renderItem={(item, firstItemInDay) => {
                        console.log('item', item);
                        return (
                            <ScheduleSingleDate 
                                shift_name={item.shift_name}
                                shift_in={item.shift_in}
                                shift_out={item.shift_out}
                            />
                        );
                    }}
                    // specify how each date should be rendered. day can be undefined if the item is not first in that day.
                    renderDay={(day, item) => {return (<View />);}}
                    // specify how empty date content with no items should be rendered
                    renderEmptyDate={() => {return (<View />);}}
                    // specify how agenda knob should look like
                    renderKnob={() => {return (<Text>View more</Text>);}}
                    // specify what should be rendered instead of ActivityIndicator
                    renderEmptyData = {() => {return (<View />);}}
                    // specify your item comparison function for increased performance
                    rowHasChanged={(r1, r2) => {return r1.text !== r2.text}}
                    // Hide knob button. Default = false
                    hideKnob={false}
                    // By default, agenda dates are marked if they have at least one item, but you can override this if needed
                    // markedDates={{
                    //     '2018-10-22': {marked: true, dotColor: '#00224B'},
                    //     '2018-10-23': {marked: true, dotColor: '#00224B'},
                    //     '2018-10-24': {marked: false, dotColor: '#00224B'},
                    //     '2018-10-25': {marked: true, dotColor: '#00224B'}
                    // }}
                    markedDates={newDate}
                    theme={{
                        selectedDayBackgroundColor: '#00224B',
                        textDayHeaderFontFamily: body4,
                        textSectionTitleColor: '#000000',
                        dayTextColor: '#000000',
                        todayTextColor: '#000000',
                    }}
                    firstDay={1}
                    
                />

            </View>
        )
    }
}

// export default ViewAllSchedule
//- redux
const mapStateToProps = (state) => ({
    scheduleList: (_.size(state.userReducer.scheduleList) > 0 && state.userReducer.scheduleList !== null) ? state.userReducer.scheduleList.data : 0
})
  
const mapDispatchToProps = (dispatch) => {
    return {
      getShiftByWeek: bindActionCreators((token) => getShiftByWeek(token), dispatch)
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ViewAllSchedule)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: bgColor
    }
});