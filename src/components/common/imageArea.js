import React, { Component } from 'react'
import { 
    Text, 
    View,
    StyleSheet,
    Image,
    TouchableOpacity
} from 'react-native'

//- color
import {
    imageAreaColor
} from '../../utils/color';

//- font
import {
    h6Font
} from '../../utils/fonts';

import PropTypes from 'prop-types';
import moment from 'moment';
import ImagePicker from 'react-native-image-crop-picker';
import {
    updateAvatar
} from '../../actions/userActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import _ from 'lodash';

export class ImageArea extends Component {

    constructor(props)
    {
        super(props)
        this.state = {
            imageURI: _.pick(this.props.userInfo.profile, 'avatar').avatar
        }
        
    }

    // _getUserAvatar = () => {
    //     this.setState({
    //         imageURI: _.pick(this.props.userInfo.profile, 'avatar').avatar
    //     })
    // }

    _onImagePress = async () => {
        console.log('On image press')
        await ImagePicker.openPicker({
            width: 400,
            height: 400,
            cropping: true,
            mediaType: 'photo',
            compressImageMaxWidth: 400,
            compressImageMaxHeight: 400,
            compressImageQuality: 0.7
        }).then(image => {
            console.log(image);
            this.setState({
                imageURI: image.path
            })
            //- update avatar
            let avatar = new FormData();
            avatar.append('avatar', {
                'uri': image.path, 
                'name': 'oclerk' + '.jpg', 
                'type': 'multipart/form-data'
            });
            this.props.updateAvatar(avatar);
        });
    }

    render() {
        const { username } = this.props;
        let uri = '';
        console.log('image in image area', this.props.userInfo);
        console.log('image uri state', this.state.imageURI)
        if(this.state.imageURI !== '')
        {   
            uri = {uri: this.state.imageURI}
        }else{
            uri = require('../../assets/images/avatar.jpg');
        }
        return (
            <View style={styles.container}>
                <TouchableOpacity 
                    onPress={() => this._onImagePress()}
                >
                    {/*<Image style={styles.image} source={uri}/>*/}
                    <Image style={styles.image} source={uri}/>
                </TouchableOpacity>
                <Text style={styles.username}>{username}</Text>
            </View>
        )
    }
}

// export default ImageArea

//- redux
const mapStateToProps = (state) => ({
    userInfo: (_.size(state.userReducer.userInfo) > 0 && state.userReducer.userInfo !== null) ? state.userReducer.userInfo.data.data : 0,
})

const mapDispatchToProps = (dispatch) => {
    return {
        updateAvatar: bindActionCreators((params, token) => updateAvatar(params, token), dispatch)
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ImageArea)

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: imageAreaColor,
    },

    image: {
        width: 120,
        height: 120,
        borderRadius: 120/2,
        overflow: 'hidden',
        alignSelf: 'center',
        marginBottom: 16,
        resizeMode: 'contain',
        marginTop: 15,
    },

    username: {
        fontFamily: h6Font,
        fontSize: 18,
        alignSelf: 'center',
        paddingBottom: 10,
    },

});

//- prop types
ImageArea.propTypes = {
    username: PropTypes.string
}

//- default
ImageArea.defaultProps = {
    username: "John William"
}
  