import React, { Component } from 'react';
import { 
    View, 
    Text,
    StyleSheet,
    Dimensions
} from 'react-native';
//- import color
import {
    bgColor
  } from '../../utils/color';
  
  //- margin
  import {
    left,
    right
  } from '../../utils/margin';
  import * as Progress from 'react-native-progress';

class SingleProgress extends Component {

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        const {
            text,
            minutes,
            percent
        } = this.props;
        return (
            <View style={styles.container}>


                    <View style={{
                        flex: 1.1,
                        flexDirection: 'column',
                        // backgroundColor: 'blue',
                    }}>
                        <View style={{flex: 1}}>
                            <Text></Text>
                        </View>
                        
                        <View style={{flex: 1}}>
                            <Text style={{justifyContent: 'flex-end', bottom:0}}>{text}</Text>
                        </View>
                    </View>

                    <View style={{
                        flex: 3,
                        flexDirection: 'column',
                        // backgroundColor: 'black'
                    }}>
                        <View style={styles.numbers}>
                            <View style={{flex: 1}}>
                                <Text style={{}}>{minutes} min</Text>
                            </View>
                            <View style={{flex: 1}}>
                                <Text style={{alignSelf: 'flex-end'}}>{percent}%</Text>
                            </View>
                        </View>
                        
                        <View style={{flex: 1}}>
                            <Progress.Bar 
                                progress={0.2} 
                                color={'#0044FF'}
                                width={237}
                                height={3}
                                style={{
                                    width: '100%',
                                    alignSelf: 'flex-end',
                                    aspectRatio: 150 / 3,
                                    // marginBottom: 10,
                                }}
                                borderColor={'white'}
                                unfilledColor={'#DADADA'}
                            />
                        </View>

                    </View>
                    
            
            </View>
        );
    }
}

export default SingleProgress;

const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'row',
        marginTop: 25,
    },
    statistic: {
        flex: 1.5,
        // backgroundColor: 'black'
    },
    // singleStatistic: {
    //     flex: 1,
    //     flexDirection: 'row',
    //     marginTop: 40,
    //     borderWidth: 1,
    // },
    numbers:{
        flex: 1,
        flexDirection: 'row',
    },

});