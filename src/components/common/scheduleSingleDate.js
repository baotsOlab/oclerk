import React, { Component } from 'react'
import { 
    Text, 
    View, 
    StyleSheet 
} from 'react-native'
import {
    left,
    right
} from '../../utils/margin';
import {
    h4Font
} from '../../utils/fonts';

export class ScheduleSingleDate extends Component {

    render() {
        const {
            shift_name,
            shift_in,
            shift_out,
        } = this.props;
        return (

            <View style={styles.container}>

                <View style={styles.singleDate}>
                    <Text style={{fontFamily: h4Font, fontSize: 16}}> {shift_name} </Text>
                </View>
                <View style={styles.singleDate}>
                    <Text style={{fontFamily: h4Font, fontSize: 16}}> Check in: {shift_in} </Text>
                </View>
                <View style={styles.singleDate}>
                    <Text style={{fontFamily: h4Font, fontSize: 16}}> Check out: {shift_out} </Text>
                </View>
                
            </View>

        )
    }

}

export default ScheduleSingleDate

const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'column',
        borderBottomWidth: 0.5,
        borderBottomColor: 'grey',
        paddingLeft: left,
        paddingTop: 22,
        paddingBottom: 22,
        borderLeftWidth: 5,
        borderLeftColor: "#44FBB4",
        backgroundColor: 'white',
        marginTop: 10,
    },
    singleDate: {
        flex: 1,
    },

});