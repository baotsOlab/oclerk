import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import { Calendar } from 'react-native-calendars';
import Header from '../common/header';
import moment from 'moment';

//- font
import {
    h5Font,
    body4
} from '../../utils/fonts';

export class Schedule extends Component {
    render() {
        const vacation = {key:'vacation', color: 'red', selectedDotColor: 'blue'};
        const massage = {key:'massage', color: 'blue', selectedDotColor: 'blue'};
        const workout = {key:'workout', color: 'green'};
        return (
            <View style={styles.container}>

                <Header
                    title={'Schedule'}
                    titleFontSize={24}
                    isDisplayWelcomeText={false}
                    isDisplayDateTimeText={false}
                    isShowAvatar={false}
                    haveBottomLine={true}
                    isShowRightHeaderTitle={true}
                    navigation={this.props.navigation}
                    rightHeaderTitleText={'view all'}
                    rightHeaderPage={'ViewAllSchedule'}
                />

                <Text style={{marginTop: 10, fontFamily: h5Font, fontSize: 16}}>
                    Week {moment().isoWeek()}
                </Text>

                <Calendar
                    markedDates={{
                        '2018-10-25': {dots: [vacation, massage, workout], selected: true, selectedColor: 'red'},
                        '2018-10-26': {dots: [massage, workout], disabled: true},
                    }}
                    markingType={'multi-dot'}
                    // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                    minDate={'2018-10-21'}
                    // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                    // maxDate={'2018-10-27'}
                    showWeekNumbers={false}
                    // firstDay={1}
                    // onDayPress={(day)=>{console.log('day pressed', day)}}
                    theme={{
                        selectedDayBackgroundColor: '#00224B',
                        textDayHeaderFontFamily: body4,
                        textSectionTitleColor: '#000000',
                        textDayHeaderFontSize: 9,
                        dayTextColor: '#000000',
                        todayTextColor: '#000000',
                    }}
                />

            
            </View>
        )
  }
}

export default Schedule

const styles = StyleSheet.create({

    container: {
        flex: 1,
    },

});