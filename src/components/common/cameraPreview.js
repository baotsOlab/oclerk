import React, { Component } from 'react'
import { 
    Text, 
    View, 
    StyleSheet,
    Image,
    TouchableOpacity,
    AsyncStorage,
    Alert
} from 'react-native'
//- import color
import {
    bgColor
} from '../../utils/color';
  
  //- margin
  import {
    left,
    right
} from '../../utils/margin';

//- import font
import {
    h6Font
} from '../../utils/fonts';
import {
    checkIn,
    checkOut,
    getShift,
    getNewShift,
    getNewShiftData
} from '../../actions/userActions';

import BasicHeader from './basicHeader';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import moment from 'moment';
import DeviceInfo from 'react-native-device-info';
import { Button } from 'react-native-elements';
import ImagePicker from 'react-native-image-crop-picker';

export class CameraPreview extends Component {

    constructor(props){
        super(props)
        this.state = {
            params: this.props.navigation.state.params,
            imageURI: this.props.navigation.state.params.imageURI,
            loading: false,
            disabled: false,
            buttonText: (this.props.navigation.state.params.type === 'checkIn') && 'Check in' || (this.props.navigation.state.params.type === 'checkOut') && 'Check out'
        }
        
    }

    _onButtonPress = async (type, currentLocation) => {
        
        
        let currentLatitude = currentLocation.coords.latitude;
        let currentLongitude = currentLocation.coords.longitude;
        
        let userToken = await AsyncStorage.getItem('userToken');
        console.log('shift id', this.props.selectedShiftID)

        // console.log('button pressed')
        if(type === 'checkIn' && this.props.selectedShiftID !== 0)
        {
            let checkInData = new FormData();
            checkInData.append('lat_in', currentLatitude);
            checkInData.append('long_in', currentLongitude);
            checkInData.append('shift_id', this.props.selectedShiftID);
            
            checkInData.append('in_image', {
                'uri': this.state.imageURI, 
                'name': type + '.jpg', 
                'type': 'multipart/form-data'
            });
            console.log('checked in...');

            await this.props.checkInAction(checkInData, userToken);
            await this.props.getNewShiftData(this.props.newShiftData)
            this.setState({
                loading: true,
                disabled: true,
                buttonText: 'Uploading...'
            })
            //- timeout 
            setTimeout(() => {
                this.setState({
                    loading: false,
                    disabled: false,
                    buttonText: (this.state.params.type === 'checkIn') && 'Check in' || (this.state.params.type === 'checkOut') && 'Check out'
                })
                this._showAlert('Check in success !');
            }, 5000);

            
        }
        else if(type === 'checkOut' && this.props.selectedShiftID !== 0)
        {
            let checkOutData = new FormData();
            checkOutData.append('lat_out', currentLatitude);
            checkOutData.append('long_out', currentLongitude);
            checkOutData.append('shift_id', this.props.selectedShiftID);
            checkOutData.append('out_image', {
                'uri': this.state.imageURI, 
                'name': type + '.jpg', 
                'type': 'multipart/form-data'
            });
            await this.props.checkOutAction(checkOutData, userToken);
            await this.props.getNewShiftData(this.props.newShiftData)
            this.setState({
                loading: true,
                disabled: true,
                buttonText: 'Uploading...'
            })
            //- timeout 
            setTimeout(() => {
                this.setState({
                    loading: false,
                    disabled: false,
                    buttonText: (this.state.params.type === 'checkIn') && 'Check in' || (this.state.params.type === 'checkOut') && 'Check out'
                })
                this._showAlert('Check out success !');
            }, 5000);

        }else{
            console.log('Please choose a shift to continue')
            this._showAlert('Please choose a shift to continue !');
        }
    }

    _getNewShift = async () => {
        let utcTime = moment().utc().format();
        let timezone = DeviceInfo.getTimezone();
        // let defaultTimezone = "Australia/Sydney";
        // let defaultUTCTime = "2018-11-05T07:26:33Z";
        let params = {
            time: utcTime,
            timezone: timezone
        }
        let userToken = await AsyncStorage.getItem('userToken');
        await this.props.getNewShift(params, userToken);
    }

    _getShift2 = async () => {
        let utcTime = moment().utc().format();
        let timezone = DeviceInfo.getTimezone();
        let userToken = await AsyncStorage.getItem('userToken');
        let data = await fetch(`http://52.187.232.212:85/api/shift?time=${utcTime}&timezone=${timezone}`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'token': userToken,
                'device-id': DeviceInfo.getUniqueID()
            },
            // body: JSON.stringify({
            //     firstParam: 'yourValue',
            //     secondParam: 'yourOtherValue',
            // }),
        }).then(response => {
            console.log('response 1', response)
            return response.json()
        }).then(reponseJson => console.log('new shift data haha.....', reponseJson));
    }

    _showAlert = (message, title = null, navigateTo = 'Main', buttonText = 'OK') => {
        Alert.alert(
            title,
            message,
            [
                {text: buttonText, onPress: () => this.props.navigation.navigate(navigateTo)},
            ],
            { cancelable: false }
        )
    }

    render() {

        const {
            currentDateTime,
            currentLocation,
            buttonText,
            type
        } = this.state.params;
        console.log('new shift data', this.props.newShiftData)
        

        console.log('type in camera preview', type)
        return (
            <View style={styles.container}>
                <BasicHeader navigation={this.props.navigation}/>
                <View style={styles.pictureArea}>
                    <Image
                        style={{width: "100%", height: "100%", borderRadius: 6}}
                        source={{uri: this.state.imageURI}}
                    />
                </View>
                <View style={styles.locationTimeArea}>
                    <Text>Checked at: {currentDateTime}</Text>
                    {/*<Text>{currentLocation}</Text>*/}
                </View>
                <View style={styles.buttonArea}>

                    {/*<TouchableOpacity 
                        onPress={() => this._onButtonPress(type, currentLocation)}
                        style={styles.buttonStyle}>
                        <Text style={styles.buttonTextStyle}>
                            {(type === 'checkIn') && 'Check in' || (type === 'checkOut') && 'Check out'}
                        </Text>
                    </TouchableOpacity>*/}

                    <Button
                        title={this.state.buttonText}
                        loading={this.state.loading}
                        disabled={this.state.disabled}
                        loadingProps={{ size: "large", color: "rgba(111, 202, 186, 1)" }}
                        titleStyle={{ fontWeight: "700" }}
                        buttonStyle={styles.buttonStyle}
                        containerStyle={{ flex: 1, marginTop: 20 }}
                        onPress={() => this._onButtonPress(type, currentLocation)}
                    />

                </View>

            </View>
        )
    }
}

// export default CameraPreview

//- redux
const mapStateToProps = (state) => ({
    selectedShiftID: (state.userReducer.selectedShiftID !== undefined) ? state.userReducer.selectedShiftID : 0,
    newShiftData: (state.userReducer.data !== undefined) ? state.userReducer.data.data : 0,
})
  
const mapDispatchToProps = (dispatch) => {
    return {
        checkInAction: bindActionCreators((params, token) => checkIn(params, token), dispatch),
        checkOutAction: bindActionCreators((params, token) => checkOut(params, token), dispatch),
        getShift: bindActionCreators((params, token) => getShift(params, token), dispatch),
        getNewShift: bindActionCreators((params, token) => getNewShift(params, token), dispatch),
        getNewShiftData: bindActionCreators((newShiftData) => getNewShiftData(newShiftData), dispatch),
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(CameraPreview)

const styles = StyleSheet.create({

    container: {
        flex: 1,
        paddingLeft: left,
        paddingRight: right,
        backgroundColor: bgColor,
    },
    pictureArea: {
        flex: 4,
        paddingTop: 10,
        paddingBottom: 5
    },
    locationTimeArea:{
        flex: 1,
    },
    buttonArea:{
        flex: 1,
    },
    buttonStyle: {
        width: '100%',
        height: 46,
        backgroundColor: '#00224B',
        // color: 'white',
        borderRadius:6,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 22
    },
    buttonTextStyle: {
        color: 'white',
        fontFamily: h6Font,
        fontSize: 16,
    }

});