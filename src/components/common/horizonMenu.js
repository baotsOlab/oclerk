import React, { Component } from 'react'
import { 
    Text, 
    View,
    FlatList,
    StyleSheet,
    TouchableOpacity,
    AsyncStorage
} from 'react-native'
import {
    element
} from '../../utils/margin';
import {
    h6Font
} from '../../utils/fonts';
import _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import {
    getShift,
    changeShift
} from '../../actions/userActions';
import moment from 'moment';
import DeviceInfo from 'react-native-device-info';


// const data = [
//     {
//         id: 1,
//         content: "Morning Shift"
//     },
//     {
//         id: 2,
//         content: "Afternoon Shift"
//     },
//     {
//         id: 3,
//         content: "Evening Shift"
//     },
//     {
//         id: 4,
//         content: "Night Shift"
//     },
// ]

export class HorizonMenu extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            selected: this.props.selectedShiftID
        };
        this._getShift();
    }

    _getShift = async () => {
        let utcTime = moment().utc().format();
        let timezone = DeviceInfo.getTimezone();
        // let defaultTimezone = "Australia/Sydney";
        // let defaultUTCTime = "2018-11-05T07:26:33Z";
        let params = {
            time: utcTime,
            timezone: timezone
        }
        let userToken = await AsyncStorage.getItem('userToken');
        await this.props.getShift(params, userToken);
        
    }

    _getShift2 = async () => {
        let utcTime = moment().utc().format();
        let timezone = DeviceInfo.getTimezone();
        let userToken = await AsyncStorage.getItem('userToken');
        let data = fetch(`http://52.187.232.212:85/api/shift?time=${utcTime}&timezone=${timezone}`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'token': userToken,
                'device-id': DeviceInfo.getUniqueID()
            },
            // body: JSON.stringify({
            //     firstParam: 'yourValue',
            //     secondParam: 'yourOtherValue',
            // }),
        });
        console.log('new shift data haha.......', data)
    }

    _onItemPressed = (id) => {
        this.setState({
            selected: id
        })
        //- force update
        this.forceUpdate();
    }

    _renderSingleItem = (item, id) => {
        let _self = this;
        // console.log('id', id);
        // console.log('shift data', item);
        // this.props.changeShift(rowData.id, rowData);
        return (
            
            <TouchableOpacity 
                style={{
                    marginRight: 20, 
                    alignItems: 'center',
                    alignContent: 'center'
                }}
                onPress={() => {
                    this._onItemPressed(item.id)
                    // console.log(item)
                    this.props.changeShift(item.id, item);
                }}
            >
                {item.id === id ?
                    <Text style={[styles.normalText, {
                        backgroundColor: '#00224B',
                        color: 'white',
                        padding: 2,
                    }]}>
                        {item.shift_name}
                    </Text>
                    :
                    <Text style={[styles.normalText]}>
                        {item.shift_name}
                    </Text>
                }
                
            </TouchableOpacity>

        );
    }

    render() {
        const {
            shiftList,
            newShiftList
        } = this.props;
        // var sl = _.map(shiftList, _.partialRight(_.pick, ['id', 'shift_name']));
        const {
            selected
        } = this.state;
        return (
            <View style={styles.container}>
                <FlatList
                    horizontal
                    data={(newShiftList !== undefined) ? newShiftList :shiftList} //- load from here
                    // data={shiftList} //- load from here
                    extraData={this.state}
                    showsHorizontalScrollIndicator={false}
                    // renderItem={({ item: rowData }) => this._renderSingleItem(rowData, this.state.selected)}
                    renderItem={({ item }) => this._renderSingleItem(item, selected)}
                    keyExtractor={(item) => item.id.toString()}
                />
            </View>
        )
    }
}

// export default HorizonMenu

//- redux
const mapStateToProps = (state) => ({
    newShiftList: state.userReducer.newShiftList,
    shiftList: state.userReducer.shiftList,
    selectedShiftID: (state.userReducer.selectedShiftID !== undefined) ? state.userReducer.selectedShiftID : 0
})
//- selectedShift must be an shift id
const mapDispatchToProps = (dispatch) => {
    return {
        getShift: bindActionCreators((params, token) => getShift(params, token), dispatch),
        changeShift: bindActionCreators((selectedShift, shiftData) => changeShift(selectedShift, shiftData), dispatch)
    }
}
  
export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(HorizonMenu)

const styles = StyleSheet.create({

    container: {
        flex: 1,
        marginTop: element + 10,
        borderBottomWidth: 0.5,
        borderBottomColor: 'grey',
        paddingBottom: 10
    },
    normal: {

    },
    normalText: {
        fontFamily: h6Font,
        color: '#ACB4BE',
        fontSize: 14,
        backgroundColor: null
    },
    selected: {},
    selectedText: {
        fontFamily: h6Font,
        color: '#ACB4BE',
        fontSize: 14,
        backgroundColor: '#00224B'
    },

});
