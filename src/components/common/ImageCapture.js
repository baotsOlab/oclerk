import React, { Component } from 'react'
import { 
    Text, 
    View,
    AsyncStorage,
    Alert,
    ActivityIndicator,
    StyleSheet,
    Button,
    Fragment,
} from 'react-native'
import ImagePicker from 'react-native-image-crop-picker';
import moment from 'moment';
import _ from 'lodash'
import {
    checkIn,
    checkOut,
    getShift,
    getNewShift,
    getNewShiftData
} from '../../actions/userActions';
//- redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'

export class ImageCapture extends Component {

    constructor(props)
    {
        super(props);
        this.state = {
            params: this.props.navigation.state.params,
            type: this.props.navigation.state.params.type,
            imageURI: '',
            navigation: this.props.navigation.state.params.navigation,
            currentDateTime: moment().format('hh:mm:ss MMMM Do, YYYY'),
            initialPosition: 'unknown',
            currentLatitude: 0,
            currentLongitude: 0,
            userToken: '',
            loading: undefined,
        }
        this._openImagePicker();
        this._getCurrentLocation();
        this._getCurrentUserToken();
    }

    _openImagePicker = async () => {
        await ImagePicker.openCamera({
            width: 350,
            height: 350,
            mediaType: 'photo',
            compressImageMaxWidth: 350,
            compressImageMaxHeight:350,
            compressImageQuality: 0.3,
            useFrontCamera: true,
        }).then(image => {

            //- get data from state
            let {
                userToken,
                imageURI,
                currentLatitude,
                currentLongitude,
                type
            } = this.state;
            this.setState({
                imageURI: image.path
            })
            console.log('image object', image)
            console.log('image source URL', image.sourceURL)
            console.log('user token', this.state.userToken)
            console.log('shift id....', this.props.selectedShiftID)

            //- check image path
            console.log('image path', this.state.imageURI)
            console.log('type', this.state.type)
            console.log('params', this.state.params)
            console.log('current latitude', this.state.currentLatitude)
            console.log('current longitude', this.state.currentLongitude)

            //- check type is checkin or checkout
            if(type === 'checkIn' && this.props.selectedShiftID !== 0)
            {
                console.log('checkin...')
                this._checkInAction(type, image.path, userToken, currentLatitude, currentLongitude);

            }
            else if(type === 'checkOut' && this.props.selectedShiftID !== 0)
            {
                console.log('checkout...')
                this._checkOutAction(type, image.path, userToken, currentLatitude, currentLongitude);
            }
            else{
                this._showAlert('Please choose a shift to continue !');
            }
            console.log('image from image picker', image);
        });
    }

    _showAlert = (message, title = null, navigateTo = 'Main', buttonText = 'OK') => {
        Alert.alert(
            title,
            message,
            [
                {text: buttonText, onPress: () => this.props.navigation.navigate(navigateTo)},
            ],
            { cancelable: false }
        )
    }

    _checkInAction = async (type, imageURI, userToken, currentLatitude, currentLongitude) => {
        //- set state to loading
        
        let checkInData = new FormData();
        checkInData.append('lat_in', currentLatitude);
        checkInData.append('long_in', currentLongitude);
        checkInData.append('shift_id', this.props.selectedShiftID);
        checkInData.append('in_image', {
            'uri': imageURI, 
            'name': type + '.jpeg', 
            'type': 'multipart/form-data'
        });
        await this.props.checkInAction(checkInData, userToken);
        await this.props.getNewShiftData(this.props.newShiftData)
        this.setState({ loading: true })
        setTimeout(() => {
            // this._showAlert('Check in success !');
            this.setState({ loading: false })
        }, 4000);
    }

    _checkOutAction = async (type, imageURI, userToken, currentLatitude, currentLongitude) => {
        //- set state to loading
        
        let checkOutData = new FormData();
        checkOutData.append('lat_out', currentLatitude);
        checkOutData.append('long_out', currentLongitude);
        checkOutData.append('shift_id', this.props.selectedShiftID);
        checkOutData.append('out_image', {
            'uri': imageURI, 
            'name': type + '.jpeg', 
            'type': 'multipart/form-data'
        });
        await this.props.checkOutAction(checkOutData, userToken);
        await this.props.getNewShiftData(this.props.newShiftData)
        this.setState({ loading: true })
        setTimeout(() => {
            // this._showAlert('Check out success !');
            this.setState({ loading: false })
        }, 4000);
    }

    _getCurrentLocation = async () => {
        //- get current location
        await navigator.geolocation.getCurrentPosition(
        (position) => {
            const initialPosition = position;
            console.log('initial position...', initialPosition.coords);
            this.setState({ 
                currentLatitude: initialPosition.coords.latitude,
                currentLongitude: initialPosition.coords.longitude   
            });
        },
        (error) => console.log('error',error.message),
        // { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );
    }

    _getCurrentUserToken = async () => {
        let userToken = await AsyncStorage.getItem('userToken');
        this.setState({
            userToken: userToken
        })
    }

    _onFinishButtonPress = () => {
        this.props.navigation.navigate("Main")
    }

    render() {
        //- load 5 seconds, then return success message
        let notification = {

        }
        return (
            <View style={styles.container}>
                {
                    (this.state.loading)
                    ?
                    <ActivityIndicator size="large" color="#00224B" />
                    :
                    (!this.state.loading)
                    ?
                    <View>
                        <Text>Uploading success !</Text>
                        <Button
                            title={'Finish'}
                            onPress={() => this._onFinishButtonPress()}
                        />
                    </View>
                    :
                    (this.state.loading === undefined)
                    &&
                    <Fragment/>
                    
                }
                
            </View>
        )
    }
}

// export default ImageCapture


//- redux
const mapStateToProps = (state) => ({
    selectedShiftID: (state.userReducer.selectedShiftID !== undefined) ? state.userReducer.selectedShiftID : 0,
    newShiftData: (state.userReducer.data !== undefined) ? state.userReducer.data.data : 0,
})
  
const mapDispatchToProps = (dispatch) => {
    return {
        checkInAction: bindActionCreators((params, token) => checkIn(params, token), dispatch),
        checkOutAction: bindActionCreators((params, token) => checkOut(params, token), dispatch),
        getShift: bindActionCreators((params, token) => getShift(params, token), dispatch),
        getNewShift: bindActionCreators((params, token) => getNewShift(params, token), dispatch),
        getNewShiftData: bindActionCreators((newShiftData) => getNewShiftData(newShiftData), dispatch),
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ImageCapture)

const styles = StyleSheet.create({

    container: {
        flex: 1,
        alignSelf: 'center',
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'center'
    },
    centerPart: {
        alignSelf: 'center',
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'center'
    }

});
