import React, { Component } from 'react'
import { 
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Alert,
    AsyncStorage
} from 'react-native'
import { 
    FormInput,
    Button
} from 'react-native-elements'
import { 
    BUTTON_MAIN_COLOR,
    LEFT_RIGHT_MARGIN
} from "../../helper/constant";
import _ from 'lodash';

//- import font
import {
    body1
} from '../../utils/fonts';

//- api
import * as api from '../../services/api';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';

//- import actions
import {
    login
} from '../../actions/userActions';

export class Input extends Component {

    constructor(props){
        super(props)
        this.state = {
            text: "hehe",
            phone: '',
            password: '',
            loading: false,
            disabled: false,
            buttonText: 'Sign in'
        }
    }

    _onLoginButtonPress = async () => 
    { 
        //- call actions
        await this.props.loginActions(this.state.phone, this.state.password);
        //- get token data
        let {
            token
        } = this.props.token;
        if(_.size(token) > 0)
        {
            // console.log('user information', this.props.userInfo)
            //- save token to asyncStorage
            await AsyncStorage.setItem('userToken', token);
            // await AsyncStorage.setItem('userInfo', this.props.userInfo);

            //- redirect to main page
            this.setState({
                loading: true,
                disabled: true,
                buttonText: 'Authenticating...'
            })
            setTimeout(() => {
                this.setState({
                    loading: false,
                    disabled: false,
                    buttonText: 'Sign in'
                })
                this.props.navigation.navigate('Main');
            }, 1000);
            
            
            
        }else{
            Alert.alert(
                'Login failed...',
                'Please check your info again !',
                [
                    {text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                ],
                { cancelable: false }
            )
        }
        
    }

    render() {
        return (
            <View style={styles.container}>

                <FormInput 
                    placeholder={'Mobile number'}
                    containerStyle={styles.containerStyle}
                    inputStyle={styles.inputStyle}
                    returnKeyType={'next'}
                    onSubmitEditing={() => { this.pass.focus(); }}
                    onChangeText={(text) => this.setState({
                        phone: text
                    })}
                />
                <FormInput 
                    placeholder={'Password'}
                    containerStyle={styles.containerStyle}
                    inputStyle={styles.inputStyle}
                    returnKeyType={'done'}
                    secureTextEntry={true}
                    ref={(input) => { this.pass = input; }}
                    onChangeText={(text) => this.setState({
                        password: text
                    })}
                />
                <Button 
                    title={this.state.buttonText}
                    loading={this.state.loading}
                    disabled={this.state.disabled}
                    borderRadius={6}
                    backgroundColor={BUTTON_MAIN_COLOR}
                    buttonStyle={styles.buttonStyle}
                    textStyle={styles.textStyle}
                    onPress={this._onLoginButtonPress}
                />
                <TouchableOpacity style={styles.recoverPass}>
                    <Text style={styles.recoverPassText}>
                        Recover password?
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }
}

// export default Input
//- redux
const mapStateToProps = (state) => ({
    token: (_.size(state.userReducer.data) > 0 && state.userReducer.data !== null) ? state.userReducer.data.data : 0,
    userInfo: (_.size(state.userReducer.data) > 0 && state.userReducer.data !== null) ? state.userReducer.data.data.user : 0,
})

const mapDispatchToProps = (dispatch) => {
    return {
        loginActions: bindActionCreators((phone, email, password) => login(phone, email, password), dispatch)
    }
}

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(Input)

const styles = StyleSheet.create({

    container:{
        flex: 1,
        width: '100%',
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },

    containerStyle: {
        borderWidth: 0.25,
        marginTop: 10,
        borderColor: '#00224B',
    },

    buttonStyle:{
        borderRadius: 0,
        marginTop: 10,
    },

    inputStyle: {
        fontSize: 14,
        paddingLeft: 10
    },

    textStyle:{
        fontSize: 16
    },

    recoverPass: {
        marginTop: 6,
        alignItems: 'center',
    },
    recoverPassText: {
        fontFamily: body1,
        fontSize: 14
    },
})