import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import reducers from './reducers/index';
import userReducer from './reducers/userReducer';

//- redux logger
import { createLogger } from 'redux-logger'
const logger = createLogger({
    // ...options
    // stateTransformer,
    // actionTransformer,
    // errorTransformer,
    // titleFormatter,
    // logErrors,
});

const store = createStore(reducers, applyMiddleware(thunk, logger));

export default store;
