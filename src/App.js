import React from 'react';
import { Provider } from 'react-redux';

import Store from './Store';
import StackNavigation from './Router';
import ErrorBoundary from './components/ErrorBoundary';

const App = () => (
    <Provider store={Store}>
        <StackNavigation />
    </Provider>
);

export default App;
