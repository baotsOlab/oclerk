import {
    FETCHING_DATA,
    FETCHING_DATA_SUCCESS,
    FETCHING_DATA_FAIL,
    SCHEDULE_LIST,
    SHIFT_LIST,
    CHANGE_SHIFT,
    LOGOUT,
    USER_INFO,
    NEW_SHIFT_LIST,
    NEW_SHIFT_DATA,
    DAY_STATISTIC
} from '../actions/types'

const initialState = {
    data: [],
    dataFetched: false,
    isFetching: false,
    error: false
}

export default function userReducer (state = initialState, action) {
    switch(action.type) {
        case FETCHING_DATA:
        {
            return {
                ...state,
                data: [],
                isFetching: true
            }
        }
        //- main area
        case FETCHING_DATA_SUCCESS:
        {
            return {
                ...state,
                data: action.data,
                isFetching: false
            }
        }
        case SCHEDULE_LIST:
        {
            return {
                ...state,
                scheduleList: action.data,
                isFetching: false
            }
        }
        case SHIFT_LIST:
        {
            // return {
            //     ...state,
            //     shiftList: action.data,
            //     isFetching: false
            // }
            return Object.assign({}, state, {
                shiftList: action.data,
                isFetching: false
            })
        }
        case CHANGE_SHIFT:
        {
            // return {
            //     ...state,
            //     // shiftData: action.data,
            //     selectedShiftID: action.selectedShiftID,
            //     shiftData: action.shiftData,
            // }
            return Object.assign({}, state, {
                selectedShiftID: action.selectedShiftID,
                shiftData: action.shiftData,
            })
        }
        case LOGOUT:
        {
            return initialState
        }
        case USER_INFO:
        {
            return {
                ...state,
                userInfo: action.userInfo
            }
        }
        case NEW_SHIFT_LIST:
        {
            return {
                ...state,
                newShiftList: action.newShiftList
            }
        }
        case NEW_SHIFT_DATA:
        {
            return {
                ...state,
                newShiftData: action.newShiftData
            }
        }
        //- end main area
        case FETCHING_DATA_FAIL:
        {
            return {
                ...state,
                error: true,
                isFetching: false
            }
        }
        case DAY_STATISTIC: 
        {
            return {
                ...state,
                dayStatistic: action.dayStatistic,
                error: true,
                isFetching: false
            }
        }
        default:
            return state;
    }
}