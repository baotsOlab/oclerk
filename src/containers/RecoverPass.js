import React, { Component } from 'react'
import { 
  Text, 
  View ,
  Button,
  StyleSheet
} from 'react-native'
import { connect } from 'react-redux';

export class RecoverPass extends Component {

  _onPress = () => {
    const { navigate } = this.props.navigation;
    navigate('Home');
  }

  render() {
    return (
      <View>
        <Text> This is recover pass page </Text>
        <Button onPress={this._onPress} title="Go to Home tab page"/>
      </View>
    )
  }
}

// export default RecoverPass

//- redux
const mapStateToProps = state => ({})

const mapDispatchToProps = {}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RecoverPass)