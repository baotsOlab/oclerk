import React, { Component } from 'react'
import { 
  View,
  StyleSheet,
  ScrollView,
  Alert,
  AsyncStorage
} from 'react-native';
import { connect } from 'react-redux';
import { List, ListItem } from 'react-native-elements'
import { bindActionCreators } from 'redux'
import {
  logout,
  getUserInfo
} from '../actions/userActions';

const list = [
  // {
  //   name: 'Employee ID',
  //   rightTitle: '1234567898761324354',
  //   hideChevron: true,
  //   disabled: true,
  // },
  {
    name: 'Username',
    rightTitle: '1234567898761324354',
    hideChevron: true,
    disabled: false,
  },
  {
    name: 'Phone number',
    rightTitle: '0123 456 789',
    hideChevron: false,
    disabled: false,
  },
  {
    name: 'Password',
    rightTitle: '*************',
    hideChevron: false,
    disabled: false,
  },
  {
    name: 'Sign out',
    rightTitle: null,
    hideChevron: false,
    disabled: false,
  }
]


//- header
import Header from '../components/common/header';

//- image area
import ImageArea from '../components/common/imageArea';

//- margin
import {
  left,
  right
} from '../utils/margin';

//- import color
import {
  bgColor
} from '../utils/color';
import _ from 'lodash';

export class Profile extends Component {

  constructor(props)
  {
    super(props)
    this.state = {
      phone: '',
      username: '',
    }
  }

  _signOut = async () => {
    let userToken = await AsyncStorage.getItem('userToken');
    console.log('async storage', userToken);
    //- signout and deactivate token
    await this.props.logoutAction(userToken);
    //- redirect to login page
    this.props.navigation.navigate('Login');
  }

  _updatePhoneNumer = (newPhone) =>
  {
    this.setState({
      phone: newPhone
    })
  }

  _updateUsername = (newUsername) =>
  {
    this.setState({
      username: newUsername
    })
  }

  render() {
    let {
      name,
      phone
    } = this.props.userInfo;
    //- find and replace phone number
    var index = _.findIndex(list, {
      name: 'Phone number'
    });
    list.splice(index, 1, {
      name: 'Phone number',
      rightTitle: (this.state.phone !== '') ? this.state.phone : phone,
      hideChevron: false,
      disabled: false,
    });
    //- find and replace username
    var index = _.findIndex(list, {
      name: 'Username'
    });
    list.splice(index, 1, {
      name: 'Username',
      rightTitle: (this.state.username !== '') ? this.state.username : name,
      hideChevron: false,
      disabled: false,
    });
    return (
      <ScrollView style={styles.container}>

        {/* Header */}
        <Header
          isShowAvatar={false}
          title={'Profile'}
          isMarginLeft={true}
          left={left}
          isMarginBottom={true}
          bottom={20}
          isDisplayWelcomeText={false}
          isDisplayDateTimeText={false}
          isMarginTop={true}
          marginTop={25}
        />

        {/* Image area */}
        <View style={styles.imageArea}>
          <ImageArea
            username={(this.state.username !== '') ? this.state.username : name}
          />
        </View>

        {/* list */}
        <View style={styles.infoList}>
          {/* list item */}
          <List containerStyle={{borderTopWidth: 0, marginTop: 0}}>
            {
              list.map((l) => (
                <ListItem
                  rightTitle={l.rightTitle}
                  key={l.name}
                  title={l.name}
                  iconRight={false}
                  disabled={l.disabled}
                  hideChevron={l.hideChevron}
                  onPress={() => {
                    if(l.name === "Sign out")
                    {
                      Alert.alert(
                        'Precaution !',
                        'Do you want to logout?',
                        [
                          {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                          {text: 'OK', onPress: () => this._signOut()},
                        ],
                        { cancelable: false }
                      )
                    }else{
                      //- navigate to edit profile
                      //- send with profile and id
                      this.props.navigation.navigate('EditProfile', {
                        userID: 1,
                        text: l.rightTitle,
                        type: l.name,
                        updatePhone: this._updatePhoneNumer,
                        updateUsername: this._updateUsername,
                      });
                    }
                  }}
                />
              ))
            }
          </List>

        </View>

      </ScrollView>
    )
  }
}

// export default Profile

//- redux
const mapStateToProps = (state) => ({
  data: (_.size(state.userReducer.data) > 0 && state.userReducer.data !== null) ? state.userReducer.data.data : 0,
  userInfo: (_.size(state.userReducer.userInfo) > 0 && state.userReducer.userInfo !== null) ? state.userReducer.userInfo.data.data : 0,
})

const mapDispatchToProps = (dispatch) => {
  return {
    logoutAction: bindActionCreators((token) => logout(token), dispatch),
    getUserInfo: bindActionCreators((token) => getUserInfo(token), dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Profile)


const styles = StyleSheet.create({

  container: {
    flex: 1,
    // paddingLeft: left,
    // paddingRight: right,
    backgroundColor: bgColor,
  },

  imageArea:{
    flex: 0.5
  },

  infoList: {
    flex: 1
  }


});