import React, { Component } from 'react'
import { 
  View,
  StyleSheet,
  Image,
} from 'react-native';
import Input from '../components/Login/input'
import { connect } from 'react-redux';

//- color
import {
  bgColor
} from '../utils/color';

//- padding
import {
	left,
	right
} from '../utils/margin';
import * as userActions from '../actions/userActions'

export class Login extends Component {

	_onPress = () => {
		const { navigate } = this.props.navigation;
		navigate('RecoverPass');
	}

	render() {
		return (
		<View style={styles.container}>

			{/* <Text> This is login page </Text>

			<Input/>
			<TouchableOpacity onPress={this._onPress}>
			<Text> Go to recover pass page </Text>
			</TouchableOpacity> */}

			<View style={styles.logo}>
				<Image source={require('../assets/images/logo.png')} />
			</View>

			<View style={styles.input}>
				<Input navigation={this.props.navigation}/>
			</View>


		</View>
		)
	}
}

// export default Login
//- redux
const mapStateToProps = (state) => ({
	
})

const mapDispatchToProps = (dispatch) => {
    return {
        
    }
}

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(Login)

const styles = StyleSheet.create({

	container: {
		flex: 1,
		flexDirection: 'column',
		backgroundColor: bgColor,

	},

	logo: {
		flex: 2,
		alignContent: 'center',
		alignItems: 'center',
		justifyContent: 'center'
	},
	input: {
		flex: 3.5,
		justifyContent: 'center',
		alignItems: 'center',
	},

})