import React, { Component } from 'react'
import { 
  Text, 
  View,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  AsyncStorage
} from 'react-native'
import moment from 'moment';
import SingleProgress from '../components/common/singleProgress';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import {
  getDayStatistic
} from '../actions/userActions';

//- font
import {
  h6Font
} from '../utils/fonts';
import { ProgressCircle }  from 'react-native-svg-charts'
import Header from '../components/common/header';
//- import color
import {
  bgColor
} from '../utils/color';

//- margin
import {
  left,
  right
} from '../utils/margin';
import DeviceInfo from 'react-native-device-info';
import _ from 'lodash';

export class Statistic extends Component {

  constructor(props)
  {
    super(props);
    this.state = {
      statistic: {},
      day: false,
      week: false,
      month: false
    }
  }

  static getDerivedStateFromProps(props, state)
  {
    console.log('new props...', props)
    console.log('new state...', state);
    if(props.dayStatistic !== state.statistic)
    {
      state.statistic = props.dayStatistic;
    }
    return state;
  }

  componentDidMount() {
    StatusBar.setHidden(true);
  }

  _onHorizoneMenuPress = async (type) => {
    let utcTime = moment().utc().format();
    let timezone = DeviceInfo.getTimezone();
    let params = {
        time: utcTime,
        timezone: timezone
    }
    let userToken = await AsyncStorage.getItem('userToken');
    if(type === 'Day')
    {
      this.props.getDayStatistic(params, userToken);
      //- set state
      this.setState({
        day: true,
        week: false,
        month: false
      })
    }
    else if(type === 'Week')
    {
      //- set state
      this.setState({
        day: false,
        week: true,
        month: false
      })
    }
    if(type === 'Month')
    {
      //- set state
      this.setState({
        day: false,
        week: false,
        month: true
      })
    }
  }

  render() {
    let {
      statistic
    } = this.state;
    let newStatistic = _.pick(statistic, ['working_time', 'in_diff', 'out_diff']);
    let {
      in_diff,
      working_time,
      out_diff
    } = newStatistic;
    console.log('this.state.statistic', this.state.statistic)
    return (
      <ScrollView style={styles.container}>

        <Header
            title={'Statistic'}
            isDisplayWelcomeText={false}
            isDisplayDateTimeText={false}
            titleFontSize={36}
            isMarginTop={true}
            marginTop={25}
            isShowAvatar={false}
            haveBottomLine={false}
            isMarginBottom={true}
            bottom={8}
          />

        <View style={styles.menu}>

            <TouchableOpacity 
              style={[styles.singleMenu, (this.state.day) && styles.active]}
              onPress={() => this._onHorizoneMenuPress('Day')}
            >
              <Text style={
                (this.state.day) ?
                {color: 'white', fontSize: 14, fontFamily: h6Font}
                :
                {color: '#979797', fontSize: 14, fontFamily: h6Font}
              }>Day</Text>
            </TouchableOpacity>

            <TouchableOpacity 
              style={[styles.singleMenu, (this.state.week) && styles.active]}
              onPress={() => this._onHorizoneMenuPress('Week')}
            >
              <Text style={
                (this.state.week) ?
                {color: 'white', fontSize: 14, fontFamily: h6Font}
                :
                {color: '#979797', fontSize: 14, fontFamily: h6Font}
              }>Week</Text>
            </TouchableOpacity>

            <TouchableOpacity 
              style={[styles.singleMenu, (this.state.month) && styles.active]}
              onPress={() => this._onHorizoneMenuPress('Month')}
            >
              <Text style={
                (this.state.month) ?
                {color: 'white', fontSize: 14, fontFamily: h6Font}
                :
                {color: '#979797', fontSize: 14, fontFamily: h6Font}
              }>Month</Text>
            </TouchableOpacity>

          </View>
      
        

        <View style={styles.graph}>
          <Text style={styles.graphHeader}>Time Archivement</Text>
          <ProgressCircle
              style={{ 
                width: 285,
                height: 285, 
                // backgroundColor: 'black',
                borderRadius: 142.5,
                marginTop: 25,
                marginBottom: 10
              }}
              progress={ 0.8 }
              progressColor={'#44FBB4'}
              strokeWidth={35}
              startAngle={0}
              endAngle={Math.PI * 2}
              backgroundColor={'#0044FF'}
          />
          <Text style={styles.subText}> {working_time} working time</Text>
        </View>

        <View style={styles.statistic}>

          <SingleProgress text={'Late work'} minutes={moment(in_diff).format('h [hour], m [minute]')} percent={0}/>
          <SingleProgress text={'Leave earlier'} minutes={moment(out_diff).format('h [hour], m [minute]')} percent={0}/>
          
        </View>

      </ScrollView>
    )
  }
}

// export default Statistic

//- redux
const mapStateToProps = state => ({
  dayStatistic: (_.size(state.userReducer.dayStatistic) > 0 && state.userReducer.dayStatistic !== null) ? state.userReducer.dayStatistic.data.data : 0,
})

const mapDispatchToProps = (dispatch) => {
  return {
    getDayStatistic: bindActionCreators((params, token) => getDayStatistic(params, token), dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Statistic)

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: bgColor,
    paddingLeft: left,
    paddingRight: right,
  },
  header: {
    flex: 1,
    // backgroundColor: 'blue'
  },
  menu: {
    flex: 1,
    // backgroundColor: 'red',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  singleMenu: {
    width: 88,
    height: 30,
    
    alignItems: 'center',
    justifyContent: 'center',
  },
  active: {
    backgroundColor: '#00224B',
  },
  graph: {
    flex: 3,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 20,
    marginBottom: 10
  },
  graphHeader: {
    fontFamily: h6Font,
    fontSize: 18,
    color: '#868E96',
    alignSelf: 'flex-start',
  },
  statistic: {
    flex: 1.5,
    // backgroundColor: 'black'
  },
  singleStatistic: {
    flexDirection: 'row',
    marginTop: 40,
  },
  subText: {
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  }

});