import React, { Component } from 'react'
import { 
    Text, 
    View,
    StyleSheet,
    TextInput,
    AsyncStorage,
    Alert,
} from 'react-native'
import _ from 'lodash';
import BasicHeader from '../components/common/basicHeader';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import {
    changePassword,
    updatePhone,
    updateUsername
} from '../actions/userActions';

//- import color
import {
    bgColor
} from '../utils/color';
  
//- margin
import {
    left,
    right
} from '../utils/margin';

export class EditProfile extends Component {

    constructor(props) {
        super(props)
        this.state = { 
            params: this.props.navigation.state.params,
            text: this.props.navigation.state.params.text,
            id: this.props.navigation.state.params.userID,
            inputName: this.props.navigation.state.params.type,
            oldPasswordPlaceholder: 'Your current password',
            newPasswordPlaceholder: 'Your new password',
            confirmPasswordPlaceholder: 'Confirm your password',
            oldPass: '',
            newPass: '',
            confirmPass: '',
            phoneNumber: this.props.navigation.state.params.text,
            username: this.props.navigation.state.params.text,
        };
        console.log('navigation state data', this.props.navigation.state)
    }

    _onButtonPress = async () => {

        //- check if text or password
        if(this.state.inputName === 'Phone number')
        {
            console.log('not password edit')
            //- update phone
            let userToken = await AsyncStorage.getItem('userToken');
            await this.props.updatePhone({
                phone: this.state.phoneNumber
            }, userToken);
            //- update phone number for parent
            this.props.navigation.state.params.updatePhone(this.state.phoneNumber)
            Alert.alert(
                null,
                'Phone number updated successfully!',
                [
                    {text: 'OK', onPress: () => this.props.navigation.goBack(), style: 'cancel'},
                ],
                { cancelable: false }
            )
            //- go back
            // this.props.navigation.goBack();

        }else if(this.state.inputName === 'Username'){
            console.log('username edit');
            //- update phone
            let userToken = await AsyncStorage.getItem('userToken');
            await this.props.updateUsername({
                name: this.state.username
            }, userToken);
            //- update username for parent
            this.props.navigation.state.params.updateUsername(this.state.username)
            Alert.alert(
                null,
                'Username updated successfully!',
                [
                    {text: 'OK', onPress: () => this.props.navigation.goBack(), style: 'cancel'},
                ],
                { cancelable: false }
            )
        }else if(this.state.inputName === 'Password'){

            console.log('password edit')
            let userToken = await AsyncStorage.getItem('userToken');
            console.log('old password', this.state.oldPass)
            console.log('new password', this.state.newPass)
            //- change password
            await this.props.changePassword({
                old_password: this.state.oldPass,
                password: this.state.newPass,
            }, userToken);
            await console.log('change password response', this.props.data);
            Alert.alert(
                '',
                'Password update success !',
                [
                    {text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                ],
                { cancelable: false }
            )
            //- go back
            this.props.navigation.goBack();

        }
    }

    _showAlert = (message, title = null, navigateTo = 'Main', buttonText = 'OK') => {
        Alert.alert(
            title,
            message,
            [
                {text: buttonText, onPress: () => this.props.navigation.navigate(navigateTo)},
            ],
            { cancelable: false }
        )
    }

    _simpleValidateNull = async () => {

    }

    render() {
        
        return (
            <View style={styles.container}>
                <BasicHeader
                    headerTitle={'Edit profile'}
                    navigation={this.props.navigation}
                    haveRightButton={true}
                    rightButtonText={'Save'}
                    rightButtonOnPress={this._onButtonPress}
                />
                

                {(this.state.inputName === "Phone number") &&
                    <View style={styles.content}>
                        <TextInput
                            style={{height: 40, borderColor: 'gray', borderBottomWidth:0.5}}
                            onChangeText={(text) => this.setState({
                                phoneNumber: text
                            })}
                            value={this.state.phoneNumber}
                            keyboardType={(this.state.inputName === "Phone number") ? 'phone-pad' : 'default'}
                            returnKeyType={'done'}
                            enablesReturnKeyAutomatically={true}
                            clearButtonMode={'unless-editing'}
                        />
                    </View>
                }

                {(this.state.inputName === "Username") &&
                    <View style={styles.content}>
                        <TextInput
                            style={{height: 40, borderColor: 'gray', borderBottomWidth:0.5}}
                            onChangeText={(text) => this.setState({
                                username: text
                            })}
                            value={this.state.username}
                            keyboardType={'default'}
                            returnKeyType={'done'}
                            enablesReturnKeyAutomatically={true}
                            clearButtonMode={'unless-editing'}
                        />
                    </View>
                }

                {(this.state.inputName === "Password") &&
                    <View style={styles.content}>
                        <TextInput
                            style={{height: 40, borderColor: 'gray', borderBottomWidth:0.5}}
                            onChangeText={(text) => this.setState({
                                oldPass: text
                            })}
                            // value={this.state.oldPasswordPlaceholder}
                            placeholder={this.state.oldPasswordPlaceholder}
                            returnKeyType={'next'}
                            //- go to next text input
                            onSubmitEditing={() => { this.newPass.focus(); }}
                            //- secure
                            secureTextEntry={true}
                            enablesReturnKeyAutomatically={true}
                        />
                        <TextInput
                            style={{height: 40, borderColor: 'gray', borderBottomWidth:0.5}}
                            onChangeText={(text) => this.setState({
                                newPass: text
                            })}
                            // value={this.state.newPasswordPlaceholder}
                            placeholder={this.state.newPasswordPlaceholder}
                            returnKeyType={'next'}
                            //- when click next
                            onSubmitEditing={() => { this.confirmPass.focus(); }}
                            ref={(input) => { this.newPass = input; }}
                            //- secure
                            secureTextEntry={true}
                            enablesReturnKeyAutomatically={true}
                        />
                        <TextInput
                            style={{height: 40, borderColor: 'gray', borderBottomWidth:0.5}}
                            onChangeText={(text) => this.setState({
                                confirmPass: text
                            })}
                            // value={this.state.confirmPasswordPlaceholder}
                            placeholder={this.state.confirmPasswordPlaceholder}
                            //- when done
                            returnKeyType={'done'}
                            ref={(input) => { this.confirmPass = input; }}
                            //- secure
                            secureTextEntry={true}
                            enablesReturnKeyAutomatically={true}
                        />
                    </View>
                }

                
            </View>
        )
    }
}

// export default EditProfile

//- redux
const mapStateToProps = (state) => ({
    data: state.userReducer.data
  })
  
const mapDispatchToProps = (dispatch) => {
    return {
      changePassword: bindActionCreators((params, token) => changePassword(params, token), dispatch),
      updatePhone: bindActionCreators((params, token) => updatePhone(params, token), dispatch),
      updateUsername: bindActionCreators((params, token) => updateUsername(params, token), dispatch)
    }
}
  
export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(EditProfile)

const styles = StyleSheet.create({

    container: {
        flex: 1,
        paddingLeft: left,
        paddingRight: right,
        backgroundColor: bgColor
    },
    content: {
        flex: 4
    }

});