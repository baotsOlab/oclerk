import React, { Component } from 'react'
import { 
  StyleSheet,
  ScrollView,
  StatusBar,
  AsyncStorage
} from 'react-native'
import { 
  MAIN_COLOR
} from "../helper/constant";
import Header from '../components/common/header';
import TabList from '../components/Home/TabList';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import {
  getUserInfo,
  getNewShift,
} from '../actions/userActions';

//- import color
import {
  bgColor
} from '../utils/color';

//- margin
import {
  left,
  right
} from '../utils/margin';
import HorizonMenu from '../components/common/horizonMenu';
import _ from 'lodash';

//- schedule
import Schedule from '../components/common/schedule';
import { NavigationEvents } from "react-navigation";
import moment from 'moment';
import DeviceInfo from 'react-native-device-info';

export class Home extends Component {

  constructor(props)
  {
    super(props);
    this.state = {
      username: this.props.userInfo.name
    }


  }

  static getDerivedStateFromProps(props, state)
  {
    console.log('new props...', props)
    console.log('new state...', state)
    return state;
  }

  componentDidMount() {
    StatusBar.setHidden(true);
    this._getUInfo();
  }

  _getUInfo = async () => {
    //- get user info
    let userToken = await AsyncStorage.getItem('userToken');
    this.props.getUserInfo(userToken);
  }

  _getShift2 = async () => {
    let utcTime = moment().utc().format();
    let timezone = DeviceInfo.getTimezone();
    let userToken = await AsyncStorage.getItem('userToken');
    let data = await fetch(`http://52.187.232.212:85/api/shift?time=${utcTime}&timezone=${timezone}`, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'token': userToken,
            'device-id': DeviceInfo.getUniqueID()
        },
        // body: JSON.stringify({
        //     firstParam: 'yourValue',
        //     secondParam: 'yourOtherValue',
        // }),
    }).then(response => {
        console.log('response 1', response)
        return response.json()
    }).then(reponseJson => console.log('new shift data haha.....', reponseJson));
  }

  _getNewShift = async () => {
    let utcTime = moment().utc().format();
    let timezone = DeviceInfo.getTimezone();
    // let defaultTimezone = "Australia/Sydney";
    // let defaultUTCTime = "2018-11-05T07:26:33Z";
    let params = {
        time: utcTime,
        timezone: timezone
    }
    let userToken = await AsyncStorage.getItem('userToken');
    await this.props.getNewShift(params, userToken);
  }

  render() {
    let {
      name,
      profile
    } = this.props.userInfo;
    let avatar = _.pick(profile, 'avatar').avatar;
    console.log('profifle data', avatar)
    return (
      <ScrollView 
        style={styles.container}
        showsVerticalScrollIndicator={true}
      >
        <NavigationEvents
          onWillFocus={payload => {
            // console.log("will focus", payload);
            this._getNewShift();
          }}
        />

        {/* header */}
        <Header
          isDisplayWelcomeText={true}
          isDisplayDateTimeText={true}
          navigation={this.props.navigation}
          isMarginTop={true}
          marginTop={25}
          userName={name}
          imageURI={avatar}
        />

        {/*<Header
          title={'Attendance'}
          isDisplayWelcomeText={false}
          isDisplayDateTimeText={false}
          titleFontSize={24}
          isMarginTop={true}
          marginTop={21}
          haveBottomLine={true}
          isShowAvatar={false}
        />*/}

        <HorizonMenu/>
        
        {/* tab lisTabt */}
        <TabList
          navigation={this.props.navigation}
        />

      </ScrollView>
    )
  }
}

// export default Home
//- redux
const mapStateToProps = (state) => ({
  userInfo: (_.size(state.userReducer.userInfo) > 0 && state.userReducer.userInfo !== null) ? state.userReducer.userInfo.data.data : 0,
})

const mapDispatchToProps = (dispatch) => {
  return {
    getUserInfo: bindActionCreators((token) => getUserInfo(token), dispatch),
    getNewShift: bindActionCreators((params, token) => getNewShift(params, token), dispatch),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home)

const styles = StyleSheet.create({

  container: {
    flex: 1,
    paddingLeft: left,
    paddingRight: right,
    backgroundColor: bgColor,
  }

})