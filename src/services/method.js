import { Platform } from 'react-native';
import _ from 'lodash';
import {
    create
} from 'apisauce';
import DeviceInfo from 'react-native-device-info';

const headers = {
    // 'Host': '',
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'device-id': DeviceInfo.getUniqueID(),
    'fcm-token': '4567890',
    'device-type': Platform.OS,
}

const api = create({
    // timeout: 30000,
    // headers: headers
})

//- api base URI
api.setBaseURL('http://52.187.232.212:85/api')

//- device id and fcm token
let deviceID = DeviceInfo.getUniqueID();
let fcm_token = '123';

export const STATUS_CODE = {
    OK: 200,
    NOT_FOUND: 404,
    BAD_REQUEST: 400,
    NO_CONTENT: 204
};

const get = async (endpoint, params = {}, token) => {
    let queryString = Object.keys(params)
        .map(key => `${key}=${params[key]}`)
        .join('&');
    if (queryString.length > 0) queryString = `?${queryString}`;
    const url = `${ api.getBaseURL() }${endpoint}${queryString}`;
    console.log('url when get', url)
    if(token !== undefined)headers.token = token;
    const response = await api.get(url, params, {
        headers: headers
    });
    // console.log(response);
    return response;
};

const post = async (endpoint, params = {}, token) => 
{
    const url = `${ api.getBaseURL() }${endpoint}`;
    //- add some extra params
    params.device_id = deviceID;
    params.fcm_token = fcm_token;
    if(token !== undefined)headers.token = token;
    const response = await api.post(url, params, {
        headers: headers
    });

    console.log(response);
    //- check if data sending is true
    if(response.ok && response.status < 400)
    {   
        return response.data;    
    }
    
};

const patch = async (endpoint, params = {}, headers = {}) => 
{
    const url = `${api.getBaseURL()}${endpoint}`;
    const response = await api.patch(url, params);
    return response;
};

const put = async (endpoint, params = {}, headers = {}) => 
{
    const url = `${api.getBaseURL()}${endpoint}`;
    const response = await api.put(url, params);
    return response;
};

const remove = async (endpoint, headers = {}) => 
{
    const url = `${api.getBaseURL()}${endpoint}`;
    const response = await api.delete(url);
    return response;
};

export { 
    get, 
    post, 
    patch, 
    remove,
    put
};
