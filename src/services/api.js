import _ from 'lodash';
import moment from 'moment';
import * as method from './method';

const login = async (params) => method.post('/auth/login', params);
const logout = async (token) => method.post('/auth/logout', {}, token);
const checkIn = async (params, token) => method.post('/attendance/check-in', params, token);
const checkOut = async (params, token) => method.post('/attendance/check-out', params, token);
const getUserInfo = async (token) => method.get('/auth/me', {}, token);
const getShiftByWeek = async (token) => method.get('/shift/get-shift-by-week', {}, token);
const changePass = async (params, token) => method.post('/auth/change', params, token);
const getShift = async (params, token) => method.get('/shift', params, token);
const getNewShift = async (params, token) => method.get('/shift', params, token);
const updatePhone = async (params, token) => method.post('/auth/update-profile', params, token);
const updateUsername = async (params, token) => method.post('/auth/update-profile', params, token);
const updateAvatar = async (params, token) => method.post('/auth/update-profile', params, token);
const getDayStatistic = async (params, token) => method.get('/attendance/stats/daily', params, token);

export {
    login,
    logout,
    checkIn,
    checkOut,
    getShiftByWeek,
    changePass,
    getShift,
    updatePhone,
    getUserInfo,
    getNewShift,
    updateUsername,
    updateAvatar,
    getDayStatistic
}